package ch.mrealg.RetrofitApi;




import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by uniflyn on 24/7/18.
 */

public interface ApiService {

  //http://jamtownapp.com/scrapper/1.php
    @FormUrlEncoded
    @POST("1.php")
    Call<CommonResponse> callurlDownload(@Field("link") String link);

}
