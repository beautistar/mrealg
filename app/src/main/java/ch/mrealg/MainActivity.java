package ch.mrealg;


import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.testfairy.TestFairy;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import ch.mrealg.RetrofitApi.ApiService;
import ch.mrealg.RetrofitApi.RetrofitSingleton;

import static com.google.android.gms.common.util.IOUtils.copyStream;

public class MainActivity extends AppCompatActivity {
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    String dirPath,currentUrl;
    WebView webview;
    private WebResourceResponse response = null;
    private String PAGEURL = "http://rdsoh.ch";
    String MY_PREFS_NAME ="mypref";
    ProgressBar progressBar;
    ApiService apiService;
    private DownloadTask mTask;
    private GoogleApiClient client;
    ProgressDialog progressDialog;
    interface DownloadCallbacks {
        void onPostExecute(String msg);
    }

    private final static int BUFFER_SIZE = 1024;
    boolean doubleBackToExitPressedOnce = false;
//    private DownloadCallbacks mCallbacks;

/*
    @Override
    protected void onStop() {
        super.onStop();
        mCallbacks =null;
    }*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        TestFairy.begin(this, "SDK-FWIOs2Bx");
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
        dirPath = Environment.getExternalStorageDirectory().toString();
        Log.e("dirpath11",dirPath);
        apiService = RetrofitSingleton.creaservice(ApiService.class);
        webview = (WebView)findViewById(R.id.web_mrealg);
        progressBar = (ProgressBar) findViewById(R.id.progress_cyclic);
        // Pre-Marshmallow warabo6277@mailseo.net akila3893



        progressBar.setMax(100);
        progressBar.setProgress(1);

        webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                Log.e("progresschar", "yes");
                progressBar.setProgress(progress);



            }
        });
        int MyVersion = Build.VERSION.SDK_INT;
        if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!checkIfAlreadyhavePermission()) {
                requestForSpecificPermission();
            }
        }

        WebSettings webSettings = webview.getSettings();

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        webSettings.setDisplayZoomControls(false);
        webSettings.setAllowFileAccess(true);
        webSettings.setBuiltInZoomControls(true);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        webview.clearCache(true);

        webview.getSettings().setAppCacheMaxSize( 5 * 1024 * 1024 ); // 5MB
        webview.getSettings().setAppCachePath( getApplicationContext().getCacheDir().getAbsolutePath() );
        webview.getSettings().setAllowFileAccess( true );
        webview.getSettings().setAppCacheEnabled( true );
        webview.getSettings().setJavaScriptEnabled( true );
        webview.getSettings().setSaveFormData(true);

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String restoredText = prefs.getString("installapp", "0");
        String root = Environment.getExternalStorageDirectory().toString();
        final File gameDir = new File(root + "/mrealgdownload");

        if (!PreferenceManager.getDefaultSharedPreferences(
                getApplicationContext())
                .getBoolean("installed", false)) {
            PreferenceManager.getDefaultSharedPreferences(
                    getApplicationContext())
                    .edit().putBoolean("installed", true).commit();



        }
        Log.e("prefermanager","asfdafsdgfdsgf");




        Log.e("restore",restoredText+"::");
        if(restoredText.equalsIgnoreCase("0")){ // newly installed app
            if(gameDir.exists()){
                Log.e("installnew","have");
                webview.setVisibility(View.GONE);
                new CopyData().execute();
            }else {
                //new Updating().execute();

                if(isNetworkAvailable()) {
                    callLocalDownload("http://rega-algorithmen.ch/Cron/2.zip");
                }
                else {
                    webview.setVisibility(View.GONE);
                    new CopyData().execute();
                    Log.e("thefolder","nocreated");
                    final AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                    alertDialog.setTitle("Fehler!");
                    alertDialog.setMessage("Keine Netzwerkverbindung");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    alertDialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
            }
        }else {
            if(gameDir.exists()){
                webview.setVisibility(View.VISIBLE);
                callFromOncreat();
                Log.e("installold","have");
            }else {
                //     new Updating().execute();
                Log.e("installold","butdonhavefile");
                if(isNetworkAvailable()) {
                    callLocalDownload("http://rega-algorithmen.ch/Cron/2.zip");
                }
                else {
                    webview.setVisibility(View.GONE);
                    Log.e("thefolder","nocreated");
                    new CopyData().execute();
                    final AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                    alertDialog.setTitle("Fehler!");
                    alertDialog.setMessage("Keine Netzwerkverbindung");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    /*if(gameDir.exists()) {

                                        alertDialog.dismiss();
                                    }else {
                                        finish();
                                    }*/
                                    alertDialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
            }
        }



/*        if(!isNetworkAvailable()) {

            Log.e("offline",gameDir+"::");
            if(gameDir.exists()){
                Log.e("thefolder","alreadycreated");





               callFromOncreat();


                //webview.loadData(newString, "text/html; charset=utf-8", "UTF-8");

//                webview.loadUrl("file://///"+dirPath+"/websitedownload/rdsoh/index.html");
            }else {
                Log.e("thefolder","nocreated");
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                alertDialog.setTitle("Fehler!");
                alertDialog.setMessage("Keine Netzwerkverbindung");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if(gameDir.exists()) {

                                    dialog.dismiss();
                                }else {
                                    finish();
                                }
                            }
                        });
                alertDialog.show();
            }



        }else {

            if(gameDir.exists()){
                Log.e("thefoldenetwr","alreadycreated");

                callFromOncreat();

             //   webview.loadUrl("file://///"+dirPath+"/mrealgdownload/rdsoh/index.html");
            }else {
                //   Log.e("online", PAGEURL);
//                 webview.loadUrl(PAGEURL);
//                new Updating().execute();
                callLocalDownload("http://rega-algorithmen.ch/Cron/2.zip");


            }
        }*/


        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString("installapp", "yes");
        editor.apply();
        //  webview.loadUrl("/storage/emulated/0/index.html");

        webview.setWebViewClient(new WebViewClient() {
            private String[] imageExtensions = {"jpg", "jpeg", "png", "gif", "JPG", "JPEG", "PNG", "GIF"};
            private Context context;

            @Override
            public void onPageFinished(WebView web, String url) {
                // TODO Auto-generated method stub

                Log.e("url",url);

                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);

                currentUrl = url;

                progressBar.setVisibility(View.VISIBLE);

            }


            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                String urlsplit="";

                Log.e("shouldoverdiload22",url+"::");

                if (!isNetworkAvailable()) {
                    currentUrl =url;


                    call6_0belowversion(currentUrl,url);


                }else {

                    String root = Environment.getExternalStorageDirectory().toString();
                    File gameDir = new File(root + "/mrealgdownload");

                    if(gameDir.exists()) {
                        call6_0belowversion(currentUrl, url);
                    }

                }




                return true;
            }


            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {


                Log.e("shouldoverdiload",request.getUrl()+"::");

                if (!isNetworkAvailable()) {
                    if (request.isForMainFrame()) {
                        currentUrl = request.getUrl().toString();

                        call7_1aboveversion(currentUrl,request);


//                    webview.loadUrl(urlsplit);


                    }


                }else {

                    String root = Environment.getExternalStorageDirectory().toString();
                    File gameDir = new File(root + "/mrealgdownload");

                    if(gameDir.exists()){

                        call7_1aboveversion(currentUrl,request);

                    }else {

                    }


                }

                return true;
            }

          /*  @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Nullable
            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {

                if (request.isForMainFrame()) {
                    currentUrl = request.getUrl().toString();

                    String urlsplit = currentUrl.replace("http://rdsoh.ch","file://///storage/emulated/0/websitedownload/rdsoh");

                    urlsplit = urlsplit+"index.html";

                    Log.e("urlsplitr",urlsplit);

                    String path = request.getUrl().getPath().toString();

                    Log.e("patheandurl",path+"::"+urlsplit);

                    response = loadFilesFromAssetFolder(path, urlsplit);

                    Log.e("response",response.getData()+"::");

                }



                return response;
            }*/
        });


        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            callMultiplePermissions();
        } else {
            // Pre-Marshmallow nena@vxmail2.net akila3893
        }




    }


    public class CopyData extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            Toast.makeText(getApplicationContext(), "Copy Started", Toast.LENGTH_LONG).show();

            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setTitle("Updating...");

            progressDialog.setIndeterminate(false);
            progressDialog.setMax(100);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            // Show ProgressBar
            progressDialog.setCancelable(false);
            //  mProgressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();




            final int totalProgressTime = 100;
            final Thread t = new Thread() {
                @Override
                public void run() {
                    int jumpTime = 0;

                    while(jumpTime < totalProgressTime) {
                        try {
                            sleep(110);
                            jumpTime += 5;
                            progressDialog.setProgress(jumpTime);
                        } catch (InterruptedException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                }
            };
            t.start();
        }

        @Override
        protected String doInBackground(Void... params) {
//            copyAssetstwpo();
            copyAssets();
            return "Done";

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Toast.makeText(getApplicationContext(), "Copy Complete", Toast.LENGTH_LONG).show();

            boolean unzipstatus =    unpackZip(Environment.getExternalStorageDirectory().toString() + "/mrealgdownload/rega.zip");


            if(unzipstatus){

                Log.e("unizip","true local");
                String root = Environment.getExternalStorageDirectory().toString() + "/mrealgdownload/rega-algorithmen.ch";
                String rootnew = Environment.getExternalStorageDirectory().toString() + "/mrealgdownload/rega";

                File file = new File(root);
                File file2 = new File(rootnew);
                boolean success;



                //copyFile(file,file2);

                if(file2.exists()) {
                    Log.e("renamee",file2.getAbsolutePath());
                    success = file.renameTo(file2);
                    Log.e("DownloadFragment", "rename rega.ch to rega" + success);
                }else {
                    Log.e("safdasfsdf","else");
                }

            }else {
                Log.e("unizip","falswe local");
            }
            progressDialog.dismiss();
            Toast.makeText(MainActivity.this, "Updated successfully!", Toast.LENGTH_SHORT).show();
            callFromOncreat();
            webview.setVisibility(View.VISIBLE);
        }
    }

    private void copyAssets() {

        InputStream in = null;
        OutputStream out = null;
        try {
            in = getAssets().open("rega.zip");

            Log.e("dzfsdf", ": "+Environment.getExternalStorageDirectory());
            File dir = new File(Environment.getExternalStorageDirectory(),
                    "mrealgdownload");
            Log.e("fsdfds1", "isExist : " + dir.exists());
            if (!dir.exists())
                dir.mkdirs();
            File fileZip = new File(dir, "rega.zip");
            Log.e("adfsdf", "isExist : " + fileZip.exists());

            out = new FileOutputStream(fileZip);
            copyFile(in, out);
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        }
        catch (IOException e) {
            Log.e("tag", "Failed to copy asset file: " + e.getMessage());
        }
    }

    private static boolean copyAsset(AssetManager assetManager,
                                     String fromAssetPath, String toPath) {
        InputStream in = null;
        OutputStream out = null;

        Log.e("copyAsset", fromAssetPath+"::"+toPath);
        try {
            in = assetManager.open(fromAssetPath);
            new File(toPath).createNewFile();
            out = new FileOutputStream(toPath);
            copyFile(in, out);
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
            return true;
        } catch(Exception e) {
            e.printStackTrace();
            Log.e("asswrewawrdea",e.getMessage());
            return false;
        }
    }

    private static void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1){
            out.write(buffer, 0, read);
        }
    }


    private void callFromOncreat() {


        //Read text from file
        StringBuilder text = new StringBuilder();
        File file = new File(dirPath+"/mrealgdownload/rega/","index.html");

        Log.e("mainoncreate",file.getAbsolutePath());

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            br.close();
        }
        catch (IOException e) {
            //You'll need to add proper error handling here

            Log.e("Exception",e.getMessage());
        }
        String newString = text.toString().replace("http://rega-algorithmen.ch/", dirPath+"/mrealgdownload/rega/");
        newString = newString.toString().replace(".js?ver", ".js%3Fver");
        newString = newString.toString().replace("@2x.png", ".png");
        Log.e("Message",newString);




        File file2 = new File(dirPath+"/mrealgdownload/rega/", "index.html");
//                String html = "<html><head><title>Title</title></head><body>This is random text.</body></html>";

        try {
            FileOutputStream out = new FileOutputStream(file2);
            byte[] data = newString.getBytes();
            out.write(data);
            out.close();
            Log.e("tghyg", "File Save : " + file2.getPath());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.e("filenot",e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("ioexce",e.getMessage());
        }


        webview.clearCache(true);

        webview.getSettings().setAppCacheMaxSize( 5 * 1024 * 1024 ); // 5MB
        webview.getSettings().setAppCachePath( getApplicationContext().getCacheDir().getAbsolutePath() );
        webview.getSettings().setAllowFileAccess( true );
        webview.getSettings().setAppCacheEnabled( true );
        webview.getSettings().setJavaScriptEnabled( true );
        webview.getSettings().setSaveFormData(true);

        webview.loadUrl("file://///"+dirPath+"/mrealgdownload/rega/index.html");

    }



    private void call6_0belowversion(String currentUrl,String url) {

        String urlsplit="";
        urlsplit = currentUrl.replace("http://rega-algorithmen.ch/", dirPath+"/mrealgdownload/rega");

        urlsplit = urlsplit + "index.html";

        Log.e("UrlLoading","urlsplitr111"+ urlsplit);

        String path = url;

        Log.e("UrlLoading","patheandurl111"+ path + "::" + urlsplit);

        Log.e("replacepath",dirPath+"/mrealgdownload/rega/"+":::"+path);

        String replacepath = path.replace(dirPath+"/mrealgdownload/rega/", "");

        Log.e("UrlLoading","replacepath11"+replacepath);


        String first = replacepath.charAt(0)+"";

        Log.e("UrlLoading","first11"+first);

        String secont = replacepath.charAt(replacepath.length()-1)+"";

        Log.e("seconstr11",secont);

        String urlreplacepath="";


        String[] separated = replacepath.split("/");


        String updatingurl;

        String replacenewpath ;

        StringBuilder sb1 = new
                StringBuilder("");


        Log.e("UrlLoading",separated.length+"::");

//                        path = path.replace("/file:","");

        Log.e("pathhjhfjdf",path);

        path = path.replace("file://","");
        Log.e("removefileinurl",path);

        StringBuilder text = new StringBuilder();
        File file = new File(path,"index.html");

        Log.e("6.0readfile",file.getAbsolutePath());

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            br.close();
        }
        catch (IOException e) {
            //You'll need to add proper error handling here

            Log.e("UrlLoadingException",e.getMessage());
        }
        String pathtwo = dirPath+"/mrealgdownload/rega/";

        Log.e("pathtwo",pathtwo);
        String newString = text.toString().replace("http://rega-algorithmen.ch/", pathtwo);
        Log.e("Message",newString);
        newString = newString.toString().replace(".js?ver", ".js%3Fver");
        newString = newString.toString().replace("@2x.png", ".png");


        File file2 = new File(path, "index.html");

//                            File file2 = new File(dirPath+"/websitedownload/rdsoh/"+path,"index.html");

        Log.e("filetwo",file2.getAbsolutePath());



//                String html = "<html><head><title>Title</title></head><body>This is random text.</body></html>";

        try {
            FileOutputStream out = new FileOutputStream(file2);
            byte[] data = newString.getBytes();
            out.write(data);
            out.close();
            Log.e("EtghygUrlLoading", "File Save : " + file2.getPath());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.e("EfilenotwriUrlLoading",e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("EioexcewriteUrlLoading",e.getMessage());
        }




        //webview.loadData(newString, "text/html; charset=utf-8", "UTF-8");

        webview.loadUrl("file:////"+path+"index.html");
        Log.e("loelse6.0UrlLoading","file:////"+path+"index.html");
    }

    private boolean checkIfAlreadyhavePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestForSpecificPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CLEAR_APP_CACHE,
        }, 101);
    }




    /*  @Override
      public void onStart() {
          super.onStart();

          // ATTENTION: This was auto-generated to implement the App Indexing API.
          // See https://g.co/AppIndexing/AndroidStudio for more information.
          client.connect();
          Action viewAction = Action.newAction(
                  Action.TYPE_VIEW, // TODO: choose an action type.
                  "Main Page", // TODO: Define a title for the content shown.
                  // TODO: If you have web page content that matches this app activity's content,
                  // make sure this auto-generated web page URL is correct.
                  // Otherwise, set the URL to null.
                  Uri.parse("http://host/path"),
                  // TODO: Make sure this auto-generated app URL is correct.
                  Uri.parse("android-app://com.ch.rdsoh/http/host/path")

          );

          //  Log.e("appstarted",PAGEURL);

          AppIndex.AppIndexApi.start(client, viewAction);
      }*/
    /*@Override
    public void onStop() {
        super.onStop();

        // We need an Editor object to make preference changes.
        // All objects are from android.context.Context
        SharedPreferences settings = getPreferences(0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("urltoload", currentUrl);

        // Commit the edits!
        editor.commit();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://ch.rdsoh/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }*/
    @Override
    public void onBackPressed() {
        //   super.onBackPressed();
        if (webview.canGoBack()) {
            webview.goBack();
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Bitte nochmals Zurück klicken, um die Applikation zu verlassen", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();



    }

    public static void unzip(InputStream zipInput, String location) throws IOException {
        try {
            File f = new File(location);
            if (!f.isDirectory()) {
                f.mkdirs();
            }
            ZipInputStream zin = new ZipInputStream(zipInput);
            try {
                ZipEntry ze = null;
                final byte[] buffer = new byte[BUFFER_SIZE];
                while ((ze = zin.getNextEntry()) != null) {
                    String path = location + ze.getName();

                    if (ze.isDirectory()) {
                        File unzipFile = new File(path);
                        if (!unzipFile.isDirectory()) {
                            unzipFile.mkdirs();
                        }
                    } else {
                        FileOutputStream fout = new FileOutputStream(path, false);
                        try {
                            int length = zin.read(buffer);
                            while (length > 0) {
                                fout.write(buffer, 0, length);
                                length = zin.read(buffer);
                            }
                            zin.closeEntry();
                        } finally {
                            fout.close();
                        }
                    }
                }
            } finally {
                zin.close();
            }
        } catch (Exception e) {
            Log.e("unzipexption", "Unzip exception", e);
        }
    }
    private void copyFromAssetsToSdcard() throws IOException {

        String root = Environment.getExternalStorageDirectory().toString();

        String mDestinationFile = root + "/southrnsss";




        String mTmpDestinationFile = root + "/southrn";

        final BufferedInputStream inputStream = new BufferedInputStream(MainActivity.this.getAssets().open("rdsoh.zip"));
        final OutputStream outputStream = new FileOutputStream(mTmpDestinationFile);
        copyStream(inputStream, outputStream);
        outputStream.flush();
        outputStream.close();
        inputStream.close();
        File tmpFile = new File(mTmpDestinationFile);
        if (tmpFile.renameTo(new File(mDestinationFile))) {
            Log.e("copyfile1", "Database file successfully copied!");
        } else {
            Log.e("copyfile2", "Database file couldn't be renamed!");
        }
    }
    private void callLocalDownload(String downloadurl) {



        String root = Environment.getExternalStorageDirectory().toString();
        File gameDir = new File(root + "/mrealgdownload");
        if (!gameDir.exists()) {
            gameDir.mkdirs();
        }

        if(gameDir.exists()){
            Log.e("directryrt","exite");
        }else {
            Log.e("notdirector","exite");
        }



        mTask = new DownloadTask();

        Log.e("dowmtask",mTask+"::"+root+"::");
        mTask.execute(downloadurl,root + "/mrealgdownload/rega.zip" );

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
 /*           case R.id.action_clear_cache:

                return true;*/
            case R.id.action_refresh:
                // User chose the "Settings" item, show the app settings UI...

                if (!isNetworkAvailable()) {

                    final AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                    alertDialog.setTitle("Fehler!");
                    alertDialog.setMessage("Keine Netzwerkverbindung");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    alertDialog.dismiss();
                                }
                            });
                    alertDialog.show();
                } else {


                    Log.e("refer","clicke");

                    String delerdsoh = Environment.getExternalStorageDirectory().toString() + "/mrealgdownload";
                    File deletef =new File(delerdsoh);
                    Log.e("refer","clicke"+deletef);
                    // deleteRecursive(deletef);

                    if(deletef.exists()) {
                        new deleteoldfile().execute();
                    }else {
                        callLocalDownload("http://rega-algorithmen.ch/Cron/2.zip");

                    }





                }


                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    private class deleteoldfile extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... strings) {

            String delerdsoh = Environment.getExternalStorageDirectory().toString() + "/mrealgdownload";
            File fileOrDirectory =new File(delerdsoh);
            Log.e("refer","clicke"+fileOrDirectory);

            if (fileOrDirectory.isDirectory()) {
                Log.e("ditrecdsf","yesss");
                for (File child : fileOrDirectory.listFiles()) {
                    Log.e("ditrecdsfSDs","yesssafdsas");
                    deleteRecursive(child);
                }
            }

            boolean isde = fileOrDirectory.delete();

            if(isde){
                Log.e("issewe","trues");


            }else {
                Log.e("issewe","false");
            }

            return "success";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            Log.e("foldderdelete","yesss"+"::"+s);

            if(s.equalsIgnoreCase("success")){
                callLocalDownload("http://rega-algorithmen.ch/Cron/2.zip");

            }else {

                Log.e("failurew","dsafdsfdsf");
            }
        }
    }

    public void deleteRecursive(File fileOrDirectory) {

        if (fileOrDirectory.isDirectory()) {
            Log.e("ditrecdsf","yesss");
            for (File child : fileOrDirectory.listFiles()) {
                Log.e("ditrecdsfSDs","yesssafdsas");
                deleteRecursive(child);
            }
        }

        boolean isde = fileOrDirectory.delete();

        if(isde){
            Log.e("issewe","trues");


        }else {
            Log.e("issewe","false");
        }
    }



    //Downloadzipfile
    private class DownloadTask extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setTitle("Updating...");

            progressDialog.setIndeterminate(false);
            progressDialog.setMax(100);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            // Show ProgressBar
            progressDialog.setCancelable(false);
            //  mProgressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();




            final int totalProgressTime = 100;
            final Thread t = new Thread() {
                @Override
                public void run() {
                    int jumpTime = 0;

                    while(jumpTime < totalProgressTime) {
                        try {
                            sleep(350);
                            jumpTime += 1;
                            progressDialog.setProgress(jumpTime);
                        } catch (InterruptedException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                }
            };
            t.start();

        }

        @Override
        protected String doInBackground(String... args) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            String destinationFilePath = "";
            try {
                URL url = new URL(args[0]);
                destinationFilePath = args[1];

                Log.e("url","urlpath"+destinationFilePath);

                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    return "Server returned HTTP " + connection.getResponseCode() + " " + connection.getResponseMessage();
                }else {
                    Log.e("faile","yess");
                }

                // download the file
                input = connection.getInputStream();

                Log.e("DownloadFragment ", "destinationFilePath=" + destinationFilePath);
                new File(destinationFilePath).createNewFile();
                output = new FileOutputStream(destinationFilePath);

                byte data[] = new byte[4096];
                int count;
                while ((count = input.read(data)) != -1) {
                    output.write(data, 0, count);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("failesadsa","yess"+e.getMessage());
                return e.toString();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (connection != null)
                    connection.disconnect();
            }

            File f = new File(destinationFilePath);

            Log.d("DownloadFragment ", "f.getParentFile().getPath()=" + f.getParentFile().getPath());
            Log.d("DownloadFragment ", "f.getName()=" + f.getName().replace(".zip", ""));
            //unpackZip(f.getParentFile().getPath(), f.getName().replace(".zip", ""));
//            unpackZip(destinationFilePath);
            return destinationFilePath;
        }

        @Override
        protected void onPostExecute(String path) {
            super.onPostExecute(path);
            Log.e("serverfromdownload ", path);
            File f = new File(path);

            if(f.exists()){

                boolean unzipstatu =   unpackZip(path);
                Log.e("unzipstatus ", unzipstatu+":::");
                if(unzipstatu){


                    String root = Environment.getExternalStorageDirectory().toString() + "/mrealgdownload/rega-algorithmen.ch";
                    String rootnew = Environment.getExternalStorageDirectory().toString() + "/mrealgdownload/rega";

                    File file = new File(root);
                    File file2 = new File(rootnew);
                    boolean success;



                    //copyFile(file,file2);

                    if(file.exists()) {
                        Log.e("fromserver",file2.getAbsolutePath());
                        success = file.renameTo(file2);
                        Log.e("DownloadFragment", " frreomserver rename rega.ch to rega" + success);


                        if(success){

                            progressDialog.dismiss();
                            Toast.makeText(MainActivity.this, "Updated successfully!", Toast.LENGTH_SHORT).show();
                            callFromOncreat();
                            webview.setVisibility(View.VISIBLE);
                        }else {
                            progressDialog.dismiss();
                            Toast.makeText(MainActivity.this, "Updated successfully!", Toast.LENGTH_SHORT).show();
                            webview.setVisibility(View.GONE);

                        }
                    }else {
                        Log.e("freomferexsda","else");
                        progressDialog.dismiss();
                        Toast.makeText(MainActivity.this, "Updated successfully!", Toast.LENGTH_SHORT).show();
                        webview.setVisibility(View.GONE);
                    }

                }else {

                    Log.e("fromserver","unizipfalse");
                    progressDialog.dismiss();
                    Toast.makeText(MainActivity.this, "Updated successfully!", Toast.LENGTH_SHORT).show();
                    webview.setVisibility(View.GONE);
                }

            }else {

                progressDialog.dismiss();
                Toast.makeText(MainActivity.this, "Updated successfully!", Toast.LENGTH_SHORT).show();
                webview.setVisibility(View.GONE);

            }




            //  mCallbacks.onPostExecute(s);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

            //  progressDialog.setProgress(values[0]);


        }
    }

    //get list of files of specific asset folder

    //get mime type by url


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    private boolean unpackZip(String filePath) {


        Log.d("DownloadFragment",filePath);
        InputStream is;
        ZipInputStream zis;
        try {

            File zipfile = new File(filePath);
            String parentFolder = zipfile.getParentFile().getPath();
            Log.d("DownloadFragment","parentFolder ::"+parentFolder);

            String filename;

            is = new FileInputStream(filePath);
            zis = new ZipInputStream(new BufferedInputStream(is));
            ZipEntry ze;
            byte[] buffer = new byte[1024];
            int count;

            while ((ze = zis.getNextEntry()) != null) {
                filename = ze.getName();

                if (ze.isDirectory()) {
                    //   Log.d("DownloadFragment","isDirectory ::"+filename);


                  /*  if(filename.contains("rdsoh.ch")) {
                        filename = filename.replace("rdsoh.ch", "rdsoh");
                        Log.d("DownloadFragment","isDirectory if ::"+filename);
                        File fmd = new File(parentFolder + "/" + filename);
                        fmd.mkdirs();
                        continue;
                    }else
                    {*/
                    // filename = filename.replace("rdsoh.ch", "rdsoh");
                    File fmd = new File(parentFolder + "/" + filename);

                    fmd.mkdirs();
                    continue;
                    /* }
                     */
                }

                FileOutputStream fout = new FileOutputStream(parentFolder + "/" + filename);
                //Log.d("DownloadFragment","childFolder ::"+filename);
                while ((count = zis.read(buffer)) != -1) {
                    fout.write(buffer, 0, count);
                }

                fout.close();
                zis.closeEntry();
            }

            zis.close();
        } catch(IOException e) {
            e.printStackTrace();
            return false;
        }


        try {


            String root = Environment.getExternalStorageDirectory().toString() + "/mrealgdownload/rega-algorithmen.ch";
            String rootnew = Environment.getExternalStorageDirectory().toString() + "/mrealgdownload/rega";

            File file = new File(root);
            File file2 = new File(rootnew);
            boolean success;




            if(file2.exists()) {
                Log.e("renamee",file2.getAbsolutePath());
                success = file.renameTo(file2);
                Log.e("DownloadFragment", "rename rega.ch to rega" + success);
            }else {
                Log.e("safdasfsdf","else");
            }
            String delezip = Environment.getExternalStorageDirectory().toString() + "/mrealgdownload/rega.zip";
            File delete =new File(delezip);
            //zipfile deletetd
            boolean del =delete.delete();
            Log.e("DownloadFragment", "delete zip" + del);

            callFromOncreat();

        }
        catch (Exception e) {
            e.printStackTrace();

            Log.e("erewrewr",e.getMessage());
        }
        return true;
    }
    private void callMultiplePermissions() {
        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();

        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("WRITE EXTERNAL STORAGE");



        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);

                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                } else {
                    // Pre-Marshmallow
                }

                return;
            }
            if (Build.VERSION.SDK_INT >= 23) {
                // Marshmallow+
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            } else {
                // Pre-Marshmallow
            }

            return;
        }

    }

    /**
     * add Permissions
     *
     * @param permissionsList
     * @param permission
     * @return
     */
    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                // Check for Rationale Option
                if (!shouldShowRequestPermissionRationale(permission))
                    return false;
            }
        } else {

            Log.e("addpermissio","notgive");
            // Pre-Marshmallow
        }

        return true;
    }

    /**
     * Permissions results
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<>();
                // Initial
                perms.put(android.Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION and others

              /*  perms.get(Manifest.permission.ACCESS_NETWORK_STATE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        &&*/

                if ( perms.get(android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                ) {
                    // All Permissions Granted



                    Log.e("permission","grandted");

                } else {
                    // Permission Denied
                    Toast.makeText(MainActivity.this, "Please Give Permission", Toast.LENGTH_SHORT)
                            .show();

                    int MyVersion = Build.VERSION.SDK_INT;
                    if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
                        if (!checkIfAlreadyhavePermission()) {
                            requestForSpecificPermission();
                        }
                    }


                    Log.e("addpermissio","notgive11");

                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    private void call7_1aboveversion(String currentUrl,WebResourceRequest request) {

        String urlsplit = "";
        String addstr = "";
        urlsplit = currentUrl.replace("http://rega.ch", "file://///" + dirPath + "/mrealgdownload/rega");

        urlsplit = urlsplit + "index.html";

        Log.e("urlsplitr", urlsplit);

        String path = request.getUrl().getPath().toString();

        Log.e("patheandurl", path + "::" + urlsplit);


        String replacepath = path.replace(dirPath + "/mrealgdownload/rega/", "");

        Log.e("replacepath", replacepath + ":::");


        if (TextUtils.isEmpty(replacepath)) {
            Log.e("replacepath","isempty");

            StringBuilder text = new StringBuilder();
            File file = new File(dirPath+"/mrealgdownload/rega/","index.html");

            Log.e("mainoncreate",file.getAbsolutePath());

            try {
                BufferedReader br = new BufferedReader(new FileReader(file));
                String line;

                while ((line = br.readLine()) != null) {
                    text.append(line);
                    text.append('\n');
                }
                br.close();
            }
            catch (IOException e) {
                //You'll need to add proper error handling here

                Log.e("Exception",e.getMessage());
            }
            String newString = text.toString().replace("http://rega-algorithmen.ch/", dirPath+"/mrealgdownload/rega/");
            newString = newString.toString().replace(".js?ver", ".js%3Fver");
            newString = newString.toString().replace("@2x.png", ".png");
            Log.e("Message",newString);




            File file2 = new File(dirPath+"/mrealgdownload/rega/", "index.html");
//                String html = "<html><head><title>Title</title></head><body>This is random text.</body></html>";

            try {
                FileOutputStream out = new FileOutputStream(file2);
                byte[] data = newString.getBytes();
                out.write(data);
                out.close();
                Log.e("tghyg", "File Save : " + file2.getPath());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Log.e("filenot",e.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("ioexce",e.getMessage());
            }

            webview.loadUrl("file://///"+dirPath+"/mrealgdownload/rega/index.html");

        }
        else {

            Log.e("replacepath", "notempty");
            String first = replacepath.charAt(0) + "";

            Log.e("first", first);

            String secont = replacepath.charAt(replacepath.length() - 1) + "";

            Log.e("seconstr", secont);

            String urlreplacepath = "";

            if (first.equalsIgnoreCase("/") && secont.equalsIgnoreCase("/")) {

                Log.e("ifcon", "yes");

                if (replacepath != null && replacepath.length() > 0) {
                    urlreplacepath = replacepath.substring(0, replacepath.length() - 1);

                    Log.e("ifconrepla", "yes" + urlreplacepath);

                }


                String[] separated = urlreplacepath.split("/");

                Log.e("ifrepla", urlreplacepath);


                Log.e("length", separated.length + "::");


                int count = 0;

                if (separated.length == 1) {
                    count = 1;
                } else {
                    count = separated.length - 1;
                }

                           /*
                            for(int i=0;i<count;i++){

                                Log.e("forrr",separated[i]);

                                // Appending the boolean value
                                addstr += "../";
//                                sb1.append("../");



                            }*/

//                            addstr += "../";

                Log.e("newreplacepathif", addstr + "::" + path);


                StringBuilder text = new StringBuilder();
                File file = new File(dirPath + "/mrealgdownload/rega/" + path, "index.html");

                try {
                    BufferedReader br = new BufferedReader(new FileReader(file));
                    String line;

                    while ((line = br.readLine()) != null) {
                        text.append(line);
                        text.append('\n');
                    }
                    br.close();
                } catch (IOException e) {
                    //You'll need to add proper error handling here

                    Log.e("Exception", e.getMessage());
                }
                String pathtwo = dirPath + "/mrealgdownload/rega/" + addstr;

                Log.e("pathtwo", pathtwo);
                String newString = text.toString().replace("http://rega-algorithmen.ch/", pathtwo);
                newString = newString.toString().replace(".js?ver", ".js%3Fver");
                newString = newString.toString().replace("@2x.png", ".png");
                Log.e("Message", newString);


                File file2 = new File(dirPath + "/mrealgdownload/rega/" + path, "index.html");

                Log.e("filetwo", file2.getAbsolutePath());
//                String html = "<html><head><title>Title</title></head><body>This is random text.</body></html>";

                try {
                    FileOutputStream out = new FileOutputStream(file2);
                    byte[] data = newString.getBytes();
                    out.write(data);
                    out.close();
                    Log.e("iftghyg", "File Save : " + file2.getPath());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Log.e("iffilenot", e.getMessage());
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("ifioexce", e.getMessage());
                }


                //webview.loadData(newString, "text/html; charset=utf-8", "UTF-8");

                webview.loadUrl("file://///" + dirPath + "/mrealgdownload/rega" + path + "index.html");
                Log.e("pathnointerne2ifff", "file://///" + dirPath + "/mrealgdownload/rega" + path + "index.html");


            } else {
                String[] separated = replacepath.split("/");


                String updatingurl;

                String replacenewpath;

                StringBuilder sb1 = new
                        StringBuilder("");


                Log.e("elselength", separated.length + "::");


                StringBuilder text = new StringBuilder();
                File file = new File(path, "index.html");

                try {
                    BufferedReader br = new BufferedReader(new FileReader(file));
                    String line;

                    while ((line = br.readLine()) != null) {
                        text.append(line);
                        text.append('\n');
                    }
                    br.close();
                } catch (IOException e) {
                    //You'll need to add proper error handling here

                    Log.e("Exception", e.getMessage());
                }
                String pathtwo = dirPath + "/mrealgdownload/rega/" + addstr;

                Log.e("pathtwo", pathtwo);
                String newString = text.toString().replace("http://rega-algorithmen.ch/", pathtwo);
                Log.e("Message", newString);
                newString = newString.toString().replace(".js?ver", ".js%3Fver");
                newString = newString.toString().replace("@2x.png", ".png");

                File file2 = new File(path, "index.html");

//                            File file2 = new File(dirPath+"/websitedownload/rdsoh/"+path,"index.html");

                Log.e("filetwo", file2.getAbsolutePath());


//                String html = "<html><head><title>Title</title></head><body>This is random text.</body></html>";

                try {
                    FileOutputStream out = new FileOutputStream(file2);
                    byte[] data = newString.getBytes();
                    out.write(data);
                    out.close();
                    Log.e("Etghyg", "File Save : " + file2.getPath());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Log.e("Efilenotwritew", e.getMessage());
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("Eioexcewrite", e.getMessage());
                }


                //webview.loadData(newString, "text/html; charset=utf-8", "UTF-8");

                webview.loadUrl("file://///" + path + "index.html");
                Log.e("pathnointerne2", "file://///" + path + "index.html");

            }


        }



    }
}

